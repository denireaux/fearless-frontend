import React, { useEffect, useState } from "react";


function ConferenceForm() {


    const [locations, setLocations] = useState([])
    const [name, setName] = useState('')
    const [starts, setStarts] = useState('')
    const [ends, setEnds] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresenters, setMaxPresenters] = useState('')
    const [maxAttendees, setMaxAttendees] = useState('')
    const [conference, setConference] = useState('')


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentersChange = (event) => {
        const value = event.target.value;
        setMaxPresenters(value);
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }


    const handleSubmit = async (event) => {

        event.preventDefault();

        const data = {}

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresenters;
        data.max_attendees = maxAttendees;
        data.location = conference;

        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers : {'Content-Type' : 'application/json'}
        }

        const response = await fetch(conferenceUrl, fetchConfig);

        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresenters('');
            setMaxAttendees('');
            setConference('')
        }
    }


    const fetchData = async () => {

        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }

    }


    useEffect(() => {
        fetchData();
    }, []); 


    return (

        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">

                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Name" name="name" required type="text" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={starts} onChange={handleStartsChange} placeholder="starts" name="starts" required type="date" id="starts" className="form-control" />
                            <label htmlFor="starts">Start Date</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={ends} onChange={handleEndsChange} placeholder="ends" name="ends" required type="date" id="ends" className="form-control" />
                            <label htmlFor="ends">End Date</label>
                        </div>

                        <div className="mb-3">
                            <textarea value={description} onChange={handleDescriptionChange} placeholder="Description" name="description" required type="textarea" id="description" className="form-control" rows="5" ></textarea>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={maxPresenters} onChange={handleMaxPresentersChange} placeholder="max_presentations" name="max_presentations" required type="number" id="max_presentations" className="form-control" />
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="max_attendees" name="max_attendees" required type="number" id="max_attendees" className="form-control" />
                            <label htmlFor="max_attendees">Max Attendees</label>
                        </div>

                        <div className="form-floating mb-3">
                            <select value={conference} onChange={handleConferenceChange} required id="location" className="form-select" name="location">
                            <option value={''}>Choose a Location</option>
                            {locations.map(location => {
                                    return (
                                        <option key={location.name} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    )
}


export default ConferenceForm;