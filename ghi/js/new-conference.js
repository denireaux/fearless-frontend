window.addEventListener('DOMContentLoaded', async () => {

    //designating url, retrieving response
    const url = 'http://localhost:8000/api/locations';
    const urlResponse = await fetch(url)
    
    if (urlResponse.ok) {

        //create data object in format {states : {name : "Alabama", abbreviation : "Al"}
        const data = await urlResponse.json();
        
        //getting the select tag
        const selectTag = document.getElementById('location');

        //stateDictionary in the format {name : "Alabama", abbreviation : "Al"}
        for (let stateDictionary of data.locations) {

            //generate the option carrier
            const option = document.createElement('option');

            option.value = stateDictionary.id;
            option.innerHTML = stateDictionary.name;
            selectTag.appendChild(option);
        }

    } else {console.log("Error")}


    //now we need the code to capture what the user has inputted

    const formTag = document.getElementById('create-conference-form');

    formTag.addEventListener('submit', async (event) => {

        event.preventDefault();

        //designating form data
        const formData = new FormData(formTag);

        //converting form data to JSON
        const json = JSON.stringify(Object.fromEntries(formData));

        //designating location url
        const locationUrl = 'http://localhost:8000/api/conferences/';

        const fetchConfig = {
            method : 'post',
            body : json,
            headers : {"Content-Type" : "application/json"}
        }

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            formTag.requestFullscreen();
            const newConference = await response.json();
            console.log(newConference)
        }


    })//end of form tage event listener







})