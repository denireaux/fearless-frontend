window.addEventListener('DOMContentLoaded', async () => {

    //designating url, retrieving response
    const url = 'http://localhost:8000/api/states/';
    const urlResponse = await fetch(url);


    if (urlResponse.ok) {

        //creating data object in format {states : {name : "Alabama", abbreviation : "Al"}
        const data = await urlResponse.json();

        //getting select tag
        const selectTag = document.getElementById('state');

        //stateDictionary in the format {name : "Alabama", abbreviation : "Al"}
        for (let stateDictionary of data.states) {

            //creating what will be our new option
            const option = document.createElement('option');

            option.value = stateDictionary.abbreviation
            option.innerHTML = stateDictionary.name
            selectTag.appendChild(option);

        }
            
            
        
    

    } else {
        console.log("error")
    }


    //programming what exactly happens when submission of form occurs
    //getting the form tag
    const formTag = document.getElementById('create-location-form');

    //listening for a submit event
    formTag.addEventListener('submit', async event => {

        //prevents default submit button behavior
        event.preventDefault();
        
        //converting form data to JSON
        //have to do so by first creating a new form data js object
        const formData = new FormData(formTag);

        //making the conversion
        const json = JSON.stringify(Object.fromEntries(formData));  //there will need to be a placeholder put in the html

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }

    });

    
})










