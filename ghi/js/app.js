function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col-4">
        <div class="card shadow-sm mb-2">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer text-muted">${startDate.toDateString()} - ${endDate.toDateString()}</div>
        </div>
    </div>
    `;
}


function displayError() {
    var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
    var alertTrigger = document.getElementById('liveAlertBtn')

    alertTrigger.style.display = "block";

    function alert(message, type) {
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

    alertPlaceholder.append(wrapper)
    }

    if (alertTrigger) {
    alertTrigger.addEventListener('click', function () {
        alert('There was an issue fetching the data!', 'failure')
    })
    }
}


window.addEventListener('DOMContentLoaded', async () => {

    //fetching the conference data from the API
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {

            //give error message
            console.error(e);
            displayError();

        } else {

            //await translating data into json
            const data = await response.json();
            
            //displaying each individual conference simultaneously
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {

                    //generating title, description, picture to pass in createCard()
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;

                    //getting picture url
                    const pictureUrl = details.conference.location.picture_url;

                    //getting the dates for the cards
                    const startDate = new Date(details.conference.starts)
                    const endDate = new Date(details.conference.ends)

                    //getting location for the card subtitles
                    const location = details.conference.location.name

                    //creating the "card"
                    const html = createCard(
                        title, 
                        description, 
                        pictureUrl, 
                        startDate, 
                        endDate,
                        location,
                        );

                    //adding the card
                    const column = document.querySelector('.row');

                    column.innerHTML += html;


                }
            }
        }

    } catch(e) {
        //displays error caught
        console.error(e)
        displayError()
    }

});